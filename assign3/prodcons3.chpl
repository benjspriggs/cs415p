module prodCons1 {
  use circQueue;

  config const numItems: int = 32;

  config const numProds: int = 2;
  config const numCons: int = 2;

  var count: atomic int;
  var consumed: atomic int;
  var lock$: sync bool;

  proc main() {
    cobegin {
      coforall i in 0..numProds-1 do
        producer(i);
      coforall i in 0..numCons-1 do
        consumer(i);
    }
  }

  proc producer(id: int) {
    var item: int = count.fetchAdd(1);

    while (item < numItems) {
      // we know we can add at least one more item
      var idx: int = add(item);
      writeln("producer ", id, " added item ", item, " to buf[", idx, "]");
      item = count.fetchAdd(1);
    }
  }

  proc consumer(id: int) {
    var pair: (int, int) = (0, 0);
    var item: int = consumed.fetchAdd(1);
    while (item < numItems) {
      pair = remove();
      writeln("consumer ", id, " removed item ", pair(2), " from buf[", pair(1), "]");
      item = consumed.fetchAdd(1);
    } 
  }
}
