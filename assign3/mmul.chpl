
module matrixMultiplication {
  config const N = 8;

  var D: domain(2) = {0..N-1, 0..N-1};
  var a, b, c: [D] int;

  proc main() {
    init();
    mult();
    validate();
  }

  // initialization
  proc init() {
    for (i, j) in D do
      a[i, j] = i + j;
    b = 1;
    c = 0;
  }

  // multiplication
  proc mult() {
    forall (i, j) in D with (+ reduce c) do
      c[i, j] = (+ reduce (a[i, 0..N-1] * b[0..N-1, j]));
  }


  // validation
  proc validate() {
    var total = (+ reduce c);

    writeln("total = ", total, " (should be 3584 iff N = 8)");
  }
}
