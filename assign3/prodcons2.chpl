module prodCons1 {
  use circQueue;

  config const numItems: int = 32;

  config const numProds: int = 2;
  config const numCons: int = 2;

  // no sync because we're validating that
  // numItems cleanly divides into the number
  // of producers and consumers we're gonna spin up

  proc main() {
    if (!is_valid_workload()) {
      writeln("invalid numProds or numCons");
      return;
    }

    cobegin {
      coforall i in 0..numProds-1 do
        producer(i);
      coforall i in 0..numCons-1 do
        consumer(i);
    }
  }

  proc is_valid_workload() {
    return (numItems/numProds) * numProds == numItems &&
            (numItems/numCons) * numCons == numItems;
  }

  proc producer(id: int) {
    var count: int = 0;
    const workUnits: int = numItems/ numProds;

    while (count < workUnits) {
      var item = (id * numProds) + count;
      writeln("producer ", id, " added item ", item, " to buf[", 
        add(item), "]");
      count += 1;
    }
  }

  proc consumer(id: int) {
    var count: int = 0;
    const workUnits: int = numItems/ numCons;
    var pair: (int, int) = (0, 0);
    while (count < workUnits) {
      pair = remove();
      writeln("consumer ", id, " removed item ", pair(2), " from buf[", pair(1), "]");
      count += 1;
    }
  }
}
