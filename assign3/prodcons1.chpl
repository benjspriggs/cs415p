module prodCons1 {
  use circQueue;

  config const numItems: int = 32;

  var itemCount$: sync int = 0;

  proc main() {
    cobegin {
      producer();
      consumer();
    }
  }

  proc producer() {
    var idx, count: int = 0;
    while (count < numItems) {
      idx = add(count);
      writeln("producer added item ", count, " to buf[", idx, "]");
      count += 1;
      itemCount$ = count;
    }

    // last flush for any consuming routines
    itemCount$ = numItems;
  }

  proc consumer() {
    var pair: (int, int) = (0, 0);
    while (itemCount$ < numItems) {
      pair = remove();
      writeln("consumer removed item ", pair(2), " from buf[", pair(1), "]");
    }
  }
}
