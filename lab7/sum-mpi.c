//------------------------------------------------------------------------- 
// This is supporting software for CS415/515 Parallel Programming.
// Copyright (c) Portland State University
//------------------------------------------------------------------------- 

// The sum program (MPI version).
//
//
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define N 64 	/* problem domain size */

int sum = 0;

int compute(int i) {
  return i*i;
}

int main(int argc, char **argv) {  
  int rank, size, dest; 
  int i, low, high, psum, sum;
  MPI_File fh;

  MPI_Init(&argc, &argv);  
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);  
  MPI_Comm_size(MPI_COMM_WORLD, &size);  

  const int partition = N/size;
  int* a = malloc(partition * sizeof(int));
  int offset = partition * rank;
  
  // all processes open and read from the same file
  MPI_File_open(MPI_COMM_WORLD, "data.txt", MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  // read our set
  MPI_File_set_view(fh, offset, MPI_INT, MPI_INT, "native", MPI_INFO_NULL);
  MPI_File_read(fh, a, partition, MPI_INT, 0);
  MPI_File_close(&fh);

  psum = 0;
  for (i = 0; i < partition; i++)
    psum += a[i];

  dest = 0;
  MPI_Reduce(&psum, &sum, 1, MPI_INT, MPI_SUM, dest, MPI_COMM_WORLD);
  if (rank == dest)
    printf("The result sum is %d\n", sum);

  MPI_Finalize();
  return 0;
}  
