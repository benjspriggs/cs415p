#!/bin/bash
gcc -g -o sum-omp0 sum-omp.c
gcc -fopenmp -S sum-omp.c
gcc -S -o sum-omp0.s sum-omp.c
wc sum-omp.s sum-omp0.s
diff sum-omp.s sum-omp0.s
