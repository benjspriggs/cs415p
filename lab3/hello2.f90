!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Hello World (hello.f90)
!
! - the .f90 extension indicates free-form source code
!   (some compilers also accept .f95 and .f03 extensions)

program hello2
print *, "Hello World!"
print *, 'Hello World!'     
print *, "Hello ", "World!" 
end program hello2
