!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Array section operations

program arraysec
parameter (N=4)
integer, dimension(1:N,1:N) :: a
integer, dimension(1:N-2,1:N-2) :: inner
integer, dimension(1:N) :: row1, row2, row3, col2, diag
integer, dimension(N*N) :: linear

a = reshape((/1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16/), (/N,N/))
row1 = a(1:N:1,1)
row2 = a(1:N,2)
row3 = a(:,3)
col2 = a(2,:)
inner = a(2:N-1,2:N-1)
linear = pack(a, .true.)
diag = linear(1:N*N:N+1)
print *, 'array a contains: '
write(*, '(4I4)') a
write(*, '(A,4I3)') 'row1 is: ', row1
write(*, '(A,4I3)') 'row2 is: ', row2
write(*, '(A,4I3)') 'row3 is: ', row3
write(*, '(A,4I3)') 'col2 is: ', col2
write(*, '(A,4I3)') 'diag is: ', diag
write(*, '(A,16I3)') 'linear is: ', linear
end
