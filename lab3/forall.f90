
        program check
          parameter(n = 8)

          integer, dimension(1:n) :: a, b, c

          a = (/ (1, i = 1, n) /)
          b = (/ (2, i = 1, n) /)
          c = (/ (3, i = 1, n) /)

          print *, "contents of a before forall: "
          write(*,*), a

          forall (i = 1:n-1)
            a(i) = b(i) + c(i)
            a(i+1) = a(i) + a(i+1)
          end forall

          print *, "contents of a after forall: "
          write(*,*), a
        end program check
