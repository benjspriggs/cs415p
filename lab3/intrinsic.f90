!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Intrinsic functions

program intrinsic
parameter (n=5)
integer, dimension(n) :: a = (/1,3,5,7,9/)
integer total, max, cnt

total = sum(a)         ! = 25
max = maxval(a)        ! = 9
cnt = count(a>3)       ! = 3
write(*,'(3I3)') total, max, cnt
end     
