! responses
      program responses
        integer, dimension(8) :: sample, C
        integer, dimension(4) :: Odd, Even, Front, Back 

        sample = (/ 3,7,4,1,8,9,5,6 /)

        Odd = sample(1:size(sample):2)
        Even = sample(2:size(sample):2)
        Front = sample(1:size(sample)/2)
        Back = sample(size(sample)/2 + 1:size(sample))
        C = sample(size(sample):1:-1)

        print *, "Sample"
        write(*, *), sample
        print *, "Odd-index elems:"
        write(*, *), Odd
        print *, "Even-index elems:"
        write(*, *), Even
        print *, "Front elems:"
        write(*, *), Front
        print *, "Back elems:"
        write(*, *), Back
        print *, "Reverse order elems:"
        write(*, *), C
      end program responses

