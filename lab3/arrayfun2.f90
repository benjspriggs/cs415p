!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Array functions

program arrayfunc2
logical, dimension(3,3) :: filter = .true.
integer, dimension(9) :: vector = (/ (i,i=1,9) /)
integer, dimension(3,3) :: a

filter(:,2) = .false.           ! [1 1 1]
print *, 'Filter:'              ! [0 0 0]
write(*, '(3B4)') filter        ! [1 1 1]


a = reshape(vector, (/3,3/))    ! [1 2 3]
print *, 'Before:'              ! [4 5 6]    
write(*, '(3I4)') a             ! [7 8 9]    

a = unpack(vector, filter, 0)   ! [1 2 3]
print *, 'After:'               ! [0 0 0]
write(*, '(3I4)') a             ! [4 5 6]
end
