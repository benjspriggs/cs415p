!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Matrix multiplication

program mm
  parameter (N = 8)
  integer, dimension(1:N,1:N) :: a, b, c

  forall (i = 1:N, j = 1:N)
     a(i,j) = i + j - 2
     b(i,j) = i - j
  end forall

  forall (i = 1:N, j = 1:N)
     c(i,j) = sum(a(i,:) * b(:,j))
  end forall

  do i = 1, N
     print *, c(i,:)
  end do

end program mm

