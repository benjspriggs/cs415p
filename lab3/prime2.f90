!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Prime finder (sequential version)

program prime2
integer i, n, next
parameter (n=100)
logical, dimension(n) :: primes = .true.
integer, dimension(n) :: ident = (/ (i,i=1,n) /)

primes(1) = .false.
next = 2
do while (next .lt. int(sqrt(real(n))))
  primes(next*2:n:next) = .false.
  next = minval(ident(next+1:n), primes(next+1:n))
end do

write(*,'(AI3)') "Primes found: ", count(primes)        
do i = 1, n
  if (primes(i)) write(*,'(I4)') i
end do
end
