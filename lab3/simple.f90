!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! A simple program

  program simple               
  i = 1
  x = 1
  print *, i, x      
  write(*,1) i, x    
1 format (I4,F8.2)   
  end
