!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Hello World (hello.f)
!
! - the .f extension indicates fixed-form source code
!   (statements must start from column 7; labels are in columns 1-5)

      program hello               
      print *, "Hello World!"
      print *, 'Hello World!'     ! single quotes are fine
      print *, "Hello ", "World!" ! multi args also work
      end program hello


