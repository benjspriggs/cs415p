!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Prime finder (sequential version)

program prime1
integer i, j, n
parameter (n=100)
logical, dimension(n) :: primes

do i = 1, n
  primes(i) = .true.
end do

primes(1) = .false.
do i = 1, int(sqrt(real(n)))
  if (primes(i)) then
    do j = i+i, n, i
      primes(j) = .false.
    end do
  end if
end do

write(*,'(AI3)') "Primes found: ", count(primes)        
do i = 1, n
  if (primes(i)) write(*,'(I4)') i
end do
end     
