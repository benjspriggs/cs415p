!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Array operations

program arrayop
parameter (N=5)
integer, dimension(1:N) :: a, b, c
data a /1,2,3,4,5/        ! array initialization

b = 2                     ! scalar expansion
c = a**2 + b**2           ! whole array operations
max = maxval(c)           ! a built-in function
write(*, '(A,5I4)') 'array c contains: ', c
write(*, '(A,I2)') 'the largest value is: ', max
end   
