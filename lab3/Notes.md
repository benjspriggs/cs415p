# Notes on Lab 3 - CS415P

## Part 1

### Group A
```
P1: 
a = 1
P2: 
x = a
b = 1
P3: 
y = b
z = a
```

### Group B
```
P1:
a = 1
y = b
sync

P2:
x = a
b = 1
sync
z = a
```

### Question 1
For group A, assume the sequential consistency (SC) model is used and all variables have an initial
value of 0. Decide which of the value combinations are possible at the end of the execution. For
each of the possible combinations, show the interleaved statement sequence that would produce the
combination.

#### Response

| x   | y   | z   | Possible?   | Statement Sequence   |
| --- | --- | --- | ----------- | -------------------- |
| 0   | 0   | 0   | yes         | 2 4 3 5 1            |
| 0   | 0   | 1   | yes         | 2 4 3 1 5            |
| 0   | 1   | 0   | no          |                      |
| 0   | 1   | 1   | yes         | 2 1 3 4 5            |
| 1   | 0   | 0   | yes         | 4 5 1 2 3            |
| 1   | 0   | 1   | yes         | 1 2 4 3 5            |
| 1   | 1   | 0   | no          |                      |
| 1   | 1   | 1   | yes         | 1 2 3 4 5            |

### Question 2
For group B under the weak consistency (WC) model, which of the value combinations are possible at
the end of the execution.

#### Response
| x   | y   | z   | Possible?   | Statement Sequence   |
| --- | --- | --- | ----------- | -------------------- |
| 0   | 0   | 0   | no          |
| 0   | 0   | 1   | yes         | 4 1 5 2 3 6 7        |
| 0   | 1   | 0   | no          |                      |
| 0   | 1   | 1   | yes         | 4 1 2 5 3 6 7        |
| 1   | 0   | 0   | no          |                      |
| 1   | 0   | 1   | yes         | 1 4 2 5 3 6 7        |
| 1   | 1   | 0   | no          |                      |
| 1   | 1   | 1   | yes         | 1 4 5 2 3 6 7        |


## Part 2
Took some time to figure out Fortran, but it has automatic array options, so it's all chill.
