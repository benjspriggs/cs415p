!------------------------------------------------------------------------- 
! This is supporting software for CS415/515 Parallel Programming.
! Copyright (c) Portland State University
!------------------------------------------------------------------------- 

! Array functions

program arrayfunc
integer, dimension(-1:2,-3:4) :: a

write(*,'(2I3)') lbound(a)  ! -1 -3
write(*,'(2I3)') ubound(a)  !  2  4
write(*,'(2I3)') shape(a)   !  4  8
write(*,'(2I3)') size(a)    ! 32

end
