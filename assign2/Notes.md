# CS415P HW2
### Benjamin Spriggs

Assignment 2 for CS415P. Available on [Gitlab](https://gitlab.com/benjspriggs/cs415p).

## General
Generate statistical results using `make stats`.
Will dump a bunch of `*.csv` files that have relevant run results included.
Will also run all of the `*.csv` files through `csvkit`, a util for simple stats on csv files.

To get it to work, *I had to disable printing*, and to enable it conditionally was a little more work than was needed for this assignment.
I've added comments at the end (suffixed `#print`) of each line that would be included in the full output.

## Notes
There was a typo in the `qsort.c` program, mis-capitalized `n`.

Going to collect stats in a CSV file and use `awk` to get me some stats.

Gonna use `csvkit` for general stats. How do I want to collect them?
Could probably aggregate them somehow. Not difficult.

Gonna have to comment out the printing statements in order to collect stats.
Not gonna worry about file I/O for this kind of an assignment.

Had to fix a unitialized pointer issue since `malloc` decides it's too good to initialize my memory for me.
