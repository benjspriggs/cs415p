#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "omp.h"

// common csv-printing methods
typedef enum { PRINT_CSV = 0, PRINT_TABBED = 1 } PrintMode;

// flag checked for in marshall_args
static const char CSV_FLAG[]= "--csv";

typedef struct row_t row_t;

// row type
struct row_t {
	int num_threads; // num of threads
	size_t core; // core id
	size_t units; // num units computed
	struct row_t* next; // next row
};

// looks for the CSV_FLAG in argv
PrintMode marshall_args(int argc, char** argv) {
	if (argc == 0) {
		return PRINT_TABBED;
	}

	for (int i = 0; i < argc; ++i) {
		if (strcmp(CSV_FLAG, argv[i]) == 0) {
			return PRINT_CSV;
		}
	}

	return PRINT_TABBED;
}

// print columns and rows from args
// nonzero return means error
int print_csv(row_t* rows) {
	printf("num_threads,core,units\n");

	for (row_t* c = rows; c != 0; c = c->next) {
		printf("%d,%lu,%lu\n", c->num_threads, c->core, c->units);
	}

	return 0;
}

// tabbed print method
int print_tabbed(row_t* rows) {
	size_t sum = 0, num_cores = 0;

	for (row_t* c = rows; c != 0; c = c->next) {
		sum += c->units;
		++num_cores;
		printf("[%2lu]: %lu, ", c->core, c->units);

		if (num_cores % 6 == 0) {
			printf("\n");
		}
	}

	printf("\n");

	printf("Total: %lu threads, %lu elements\n", num_cores, sum);

	return 0;
}

int print_results(int argc, char** argv, row_t* rows) {
	switch (marshall_args(argc, argv)) {
		case PRINT_CSV:
			return print_csv(rows);
		case PRINT_TABBED:
			return print_tabbed(rows);
	}

	return 1;
}

#define COLLECT(rows, core, units) \
	_Pragma("omp critical") \
{ \
	rows = submit_result((rows), (core), (units)); \
}

#define FINISH(total_expected) \
  if (print_results(argc, argv, rows) || total != total_expected) { \
    printf("total = %d, expected %d\n", total, total_expected); \
    return 1; \
  }

row_t* submit_result(row_t* rows, size_t core, size_t units) {
	row_t* r = malloc(sizeof(row_t));

	r->num_threads = omp_get_num_threads();
	r->core = core;
	r->units = units;
	r->next = 0;

	if (rows == 0) {
		return r;
	}

	if (core < rows->core) {
		r->next = rows;
		return r;
	}

	row_t* p = rows, *c = rows->next;

	while (c != 0 && c->core < core) {
		p = c;
		c = c->next;
	}

	if (c == 0) {
		if (p->core > core) {
			r->next = p;
			return r;
		} else if (p->core == core) {
			p->units += units;
			return rows;
		} else {
			p->next = r;
			return rows;
		}
	}


	if (c->core == core) {
		c->units += units;
		return rows;
	}

	if (p->core == core) {
		p->units += units;
		return rows;
	}

	if (c->core > core) {
		p->next = r;
		r->next = c;
	}

	return rows;
}
