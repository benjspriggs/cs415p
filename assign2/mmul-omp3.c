//------------------------------------------------------------------------- 
// This is supporting software for CS415P/515 Parallel Programming.
// Copyright (c) Portland State University.
//------------------------------------------------------------------------- 

// Matrix multiplication algorithm.  
//
#include <stdio.h>
#include "omp.h"
#include "common.h"

#define N 64

int main(int argc, char** argv) {
  int a[N][N], b[N][N], c[N][N];
  int i, j, k;

  // initialization
  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      a[i][j] = i + j;
      b[i][j] = 1;
    }
  }

  row_t * rows = 0;

  // multiplication
  for (i = 0; i < N; i++) {
#pragma omp parallel private(j, k)
    {
      size_t core = omp_get_thread_num();
      size_t units = 0;
      // printf("c#%lu [%d, %d]\n", core, i, j); // #print

#pragma omp for
      for (j = 0; j < N; j++) {
        c[i][j] = 0.;
        for (k = 0; k < N; k++) {
#pragma omp critical
          c[i][j] += a[i][k] * b[k][j];
        }
        units += 1;
      }

      COLLECT(rows, core, units);
    }
  }

  // validation
  int total = 0;
  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      total += c[i][j];
    }
  }

FINISH(16515072)
}
