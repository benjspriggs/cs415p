//------------------------------------------------------------------------- 
// This is supporting software for CS415/515 Parallel Programming.
// Copyright (c) Portland State University.
//------------------------------------------------------------------------- 

// Locale Example 4
//

use Random;

var rs = new RandomStream(int);

var x: int = rs.getNext(0, numLocales - 1);
on Locales(x) {
  var y: int = rs.getNext(0, numLocales - 1);
  on Locales(y) {
    var z = x;
    writeln("x's locale: " + x.locale.id);
    writeln("y's locale: " + y.locale.id);
    writeln("z's locale: " + z.locale.id);
  }
} 
