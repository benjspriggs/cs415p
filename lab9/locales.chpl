
forall locale in Locales {
on locale do {
writeln("Locale "+ here.id + ": " + here.name + " (with " + here.numPUs(false,true) + " cores)");
}
}
