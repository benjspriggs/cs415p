//------------------------------------------------------------------------- 
// This is supporting software for CS415/515 Parallel Programming.
// Copyright (c) Portland State University.
//------------------------------------------------------------------------- 

// Domain Map Example 
//
// linux> ./domMap1 -nl 4
//

use BlockDist;
use CyclicDist;

config const n = 16;
const D = {1..n};
const BD = D dmapped Block(D);
const CD = D dmapped Cyclic(D.low);
const CD2 = D dmapped Cyclic(4);

var a: [D] int = [x in D] x.locale.id;
var b: [BD] int = [x in BD] x.locale.id;
var c: [CD] int = [x in CD] x.locale.id;
var c2: [CD2] int = [x in CD2] x.locale.id;

write("a:  "); writeln(a);
write("b:  "); writeln(b);
write("c:  "); writeln(c);
write("c2: "); writeln(c2);
