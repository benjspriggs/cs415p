//------------------------------------------------------------------------- 
// This is supporting software for CS415/515 Parallel Programming.
// Copyright (c) Portland State University.
//------------------------------------------------------------------------- 

// Hello world program (OMP version).
//
#include <stdio.h>
#include <omp.h>

int main() {
#pragma omp parallel
  printf("Hello world! -- thread %d\n", omp_get_thread_num());
}
