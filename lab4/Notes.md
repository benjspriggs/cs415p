# Lab 4 Notes
### Benjamin Spriggs

## General

### a
Briefly explain the following parallel architectures: SIMD and SMP. For each one, describe the
typical programming approach(es).

#### Response
SIMD stands for 'Single instruction, multiple data', SMP stands for 'Symmetrical multi-processing'.

SMP is used in most multicore systems, since each processor is treated equally, and each processor has equal treatment and works on the problem in tandem. You can coordinate resources w/ locks and other mechanisms.

SIMD is where you do a single instruction on multiple pieces of data in parallel, not necessarily concurrently but exploiting the benefits of data parallelism.

### b
Describe the main programming issues in shared memory programming.

### Response
One of the main programming issues in shared memory programming is coordinating resources and critical sections of code, and all of the other constraints on this main problem.

## Pthreads
Consider the following Pthreads program:

```c
void g() { printf("G\n"); }
void f(int *ip) {
  pthread_t t3;
  pthread_create(&t3, NULL, (void *)g, NULL);
  printf("f %d\n", *ip);
  pthread_join(t3, NULL);
}
int main() {
  int i=1, j=2;
  pthread_t t1, t2;
  pthread_create(&t1, NULL, (void *)f, &i);
  pthread_create(&t2, NULL, (void *)f, &j);
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  printf("Work done\n");
}
```

### a
What are possible outputs of this program? Either describe or enumerate.

#### Response

So we have an `f()` and a `g()` who can operate in any order. We know that the `"Work done"` string will be output at the end, and it's just the order of the `f()` and `g()` that change.

```
G
f 1
G
f 2
Work done
```

Or:

```
G
G
f 1
f 2
Work done
```

Or similar, based on which thread is serviced first. `G`s will always come before `f #`.

### b
What are the purpose of those `pthread_join` calls? What happens if they are removed?

#### Response

When `pthread_join` is used on a joinable thread, the parent thread waits for the child to execute, and marks all of the resources used by the child as unused when it's done.

If the calls are removed, any thread or routine that calls `pthread_create` will just continue on as if nothing has happened, and not wait for the child to finish. There's also the issue of unfreed resources that will linger as long as the program is in use.

## OpenMP
Each of the following programs contains an OpenMP-related problem. For A, the compile
indicates an OpenMP directive error; for B, the problem is that the directives do not seem to have any
effect on the program’s performance. Identify the error in each program.

### A
```c
#define N 128

int main(int argc, char **argv) {
  int i, tid, a[N];

  #pragma omp parallel for
  {
    tid = omp_get_thread_num();
    printf("tid=%d\n", tid);
    for (i=0; i<N; i++)
      a[i] = i;
  }
}
```

#### Response
The error here is that `#pragma omp parallel for` isn't attached to a `for` block.

### B
```c
// ... other routines omitted ...

void quicksort (int* array, int left, int right) {
  if (left >= right) return;
  int middle = partition(array, left, right);
  #pragma omp task
  quicksort(array, left, middle-1);
  quicksort(array, middle+1, right);
  #pragma omp taskwait
}
int main(int argc, char **argv) {
  int N, *array;
  array-Init(array);
  quicksort(array, 0, N-1);
}
```

#### Response
A task is created, but never called in parallel.

## Data-parallel
Fortran 90/95 has two data-parallel programming mechanisms, array (section) operations and the forall statement.

### a
Implement the following array operation by using a `forall` statement:

```fortran
a = b(1:n) + c(n:1:-1)
```

#### Response
```fortran
integer, dimension(n) :: a, b, c

forall (i = 1:n-1)
  a(i) = b(i) + c(n-i)
end forall
```

### b
Implement the following `forall` statement by using array operations:

```fortran
integer, dimension(n) :: a, b, c
forall (i = 1:n-1)
  a(i) = b(i) + c(i)
  a(i+1) = a(i) + a(i+1)
end forall
```

#### Response

### c
Do you think these two mechanisms have equal power, i.e. anything expressible in one is express-
ible in the other? Provide a good argument or a convincing example.

#### Response
`forall` is just an array operation under the hood, afaik.

## General
Consider the producer-consumer program you wrote in Assignment 1. We want to write a C
program to simulate the behavior of this program. The C program generates non-deterministic results
— different runs may produce different sequences of add-task and remove-task actions.

### a
What enables a C program to generate non-deterministic results?

### b
Give a sketch for the C program.

### c
Can your program generate any sequence of add-task and remove-task that a Pthread program
can?

### d
Does your program generate different sequences with the same frequency pattern as a Pthread
program does?
