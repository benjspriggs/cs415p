// mmul for chapel

config const N = 8;

const D = {0..N-1,  0..N-1};

var a, b, c: [D] int;

// initialization
forall i in D {
	for j in D {
		a[i,j] = i + j;
		b[i,j] = 1;
	}
}

// https://chapel-lang.org/docs/1.14/technotes/reduceIntents.html
// multiplication
for (i = 0; i < N; i++) {
	for (j = 0; j < N; j++) {
		c[i][j] = 0.;
		for (k = 0; k < N; k++) {
			c[i][j] += a[i][k] * b[k][j];
		}
	}
}

// validation
int total = 0;
for (i = 0; i < N; i++) {
	for (j = 0; j < N; j++) {
		total += c[i][j];
	}
}
printf("total = %d (should be 3584)\n", total);

