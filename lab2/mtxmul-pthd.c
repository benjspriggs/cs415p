//------------------------------------------------------------------------- 
// This is supporting software for CS415/515 Parallel Programming.
// Copyright (c) Portland State University
//------------------------------------------------------------------------- 

// Matrix multiplication algorithm.  
//
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define N 16

int a[N][N], // first array
    b[N][N], // second array
    c[N][N]; // stored sum

void init_array()  {
  int i, j;
  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      a[i][j] = i + j;
      b[i][j] = N - j;
    }
  }
}

// compute_args_t
typedef struct {
  int row;
  int column;
} compute_args_t;

// compute c[row][column] by:
//  - multiplying each a[row] by b[column]
//  - summing the results
//  - storing into array c
void compute_entry(void* ctx) {
  compute_args_t* c_args = (compute_args_t*) ctx;

  if (c == NULL) {
    return;
  }

  int entry = 0;

  for (int i = 0; i < N; ++i) {
    entry += a[c_args->row][i] * b[i][c_args->column];
  }

  c[c_args->row][c_args->column] = entry;
}

typedef struct {
  int start; // 1..N*N starting index
  int partition_size;
} entries_arg_t;

void compute_entries(void* ctx) {
  entries_arg_t* e = (entries_arg_t*)ctx;

  if (e == NULL)
    return;

  pthread_t* ent = malloc(e->partition_size * sizeof(pthread_t));

  for (int i = e->start; i < e->partition_size; ++i) {
    compute_args_t* c = malloc(sizeof(compute_args_t));
    c->row = i / N;
    c->column = i % N;
    pthread_create(&ent[i], NULL, (void*) compute_entry, (void*) c);
  }

  for (int i = e->start; i < e->partition_size; ++i) {
    pthread_join(ent[i], NULL);
  }
}

void print_array()  {
  int i, j;
  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++)
      printf("%4d ", c[i][j]);
    printf("\n");
  }
}

int main(int argc, char **argv) {
  // marshall args
  if (argc < 0 || argc > 2) {
    return -1;
  }

  int num_threads = 1;

  if (argc == 2) {
    num_threads = atoi(argv[1]);
  }

  init_array();

  // number of entries each thread computes
  // (last thread gets the extra work)
  int partition_size = (N * N) / num_threads;
  entries_arg_t context = { 0, partition_size, };

  compute_entries(&context);

  print_array();
}
