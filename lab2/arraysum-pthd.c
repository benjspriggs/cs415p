#define _GNU_SOURCE
//------------------------------------------------------------------------- 
// This is supporting software for CS415/515 Parallel Programming.
// Copyright (c) Portland State University
//------------------------------------------------------------------------- 

// A simple array-sum program using Pthreads.
//
// Usage: ./arraysum-pthd
//
//
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <unistd.h>

#ifndef NO_LOCKS
#define LOCKS
#endif

int arraySize = 1000;           // default array size 
int numThreads = 10;            // default number of threads 
int *array;                     // shared array 
int sum = 0, idx = 0;           // global sum and idx 
pthread_mutex_t sumLock;   

// Initialize array to [1,2,...,size]
//
int *init_array(int size) {
  int *array = (int *) malloc(sizeof(int) * size);
  for (int i = 0; i < size; i++)
    array[i] = i + 1;
  printf("Initialized array to [1,2,...,%d]\n", size);
  return array;
}

// The worker routine
//
void slave(long tid) {
#ifdef LOCKS
  printf("Thread %ld started on core %d\n", tid, sched_getcpu());
#else
  printf("Thread %ld started\n", tid);
#endif

  int i, psum = 0;
  do {
#ifdef LOCKS
    pthread_mutex_lock(&sumLock);       // read and increment idx
#endif
		// printf("Incrementing i = %d, idx = %d\n", i, idx);
    i = idx++;                        
#ifdef LOCKS
    pthread_mutex_unlock(&sumLock);     
#endif
    if (i < arraySize)                  // add one array element
      psum += array[i];
  } while (i < arraySize);

#ifdef LOCKS
  pthread_mutex_lock(&sumLock);         // add local psum to global sum 
#else
	// debug
	printf("#SUM Incrementing sum %d\n", sum);
	printf("Thread %ld incrementing with psum %d\n", tid, psum);
#endif
  sum += psum;
#ifdef LOCKS
  pthread_mutex_unlock(&sumLock);
#endif
} 

// The main routine
//
int main(int argc, char **argv) {
	// validate arguments
	if (argc > 3 || argc < 1) {
		return -1;
	}

	// parse arguments
	switch (argc) {
		case 3:
			// set number of threads
			numThreads = atoi(argv[2]);
		case 2:
			// set array size
			arraySize = atoi(argv[1]);
	}

	// validate parsed arguments
	if (numThreads < 0 || arraySize < 0) {
		return -1;
	}

  pthread_t thread[numThreads];
  array = init_array(arraySize); 	// initialize array 
  pthread_mutex_init(&sumLock, NULL);   // initialize mutex 

	// affinity-setting
	int nprocs = sysconf(_SC_NPROCESSORS_ONLN);
	cpu_set_t cpuset;
	int cid = 0;

  for (long k = 0; k < numThreads; k++) {        // create threads 
    pthread_create(&thread[k], NULL, (void*)slave, (void*)k);
		CPU_ZERO(&cpuset);
		CPU_SET(cid++ % nprocs, &cpuset);
		pthread_setaffinity_np(thread[k], sizeof(cpu_set_t), &cpuset);
  }
  printf("%d threads created\n", numThreads);

  for (long k = 0; k < numThreads; k++)          // join threads 
    pthread_join(thread[k], NULL);
  printf("%d threads joined\n", numThreads);

  printf("The sum of 1 to %i is %d\n", arraySize, sum);
}  
