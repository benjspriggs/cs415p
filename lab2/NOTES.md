# Notes on Lab2
## Exercise 2
We're looking to find a race condition in our code, without locking mechanisms in place.

Let's make a `make` target to find it, and go through.

### Runs
We can do a run with this: `make evidence | sort` and see lines where sum is 0 multiple times, implying a race condition.
