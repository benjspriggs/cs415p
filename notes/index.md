---
title: CS415 Notes - Benjamin Spriggs
header-includes:
	<link rel="stylesheet" href="github-pandoc.css" >
---

# Frontmatter

## Index

- [Lectures](lecture/)
- [Reference](reference/)
- [Resources](resources/)

## Rules

Open book, open notes.

Electronic devices only allowed for accessing lecture notes and language manuals on D2l.

<nav id="toc" data-toggle="toc"></nav>

# Week 1 - Introduction

## Slides
- [Admin](lecture/admin-2up.pdf)
- [Introduction](lecture/intro-2up.pdf)

Lots of history. There are multiple layers that go to the different dimensions of parallel computing at different levels of complexity.

## CPU with Vector Extensions

Limited functions that are overloaded, based on usages. Choose to overload FPUs as vector units, get the same operations w/ less efficiency but more GPU power in the CPU.

### Vector Processors

Do instructions that process a sequence of data w/ single instruction. Power derived from heavily pipelined architecture.

Makes it simple to program, just mark out the ‘parallelizeable’ segments (inner loops).

## Multicore Processors

Multiple cores that compute with shared cache. Parallelism is implemented at OS level, programs can pretend they’re still single core, or OS can expose thread interface.

### Manycore Processors

OSes can’t do all that work. SMP and related systems handle programs of 64+ cores.

## Hybrid CPU/ GPU Systems

CPU does OS stuff, GPU does a bunch of calculations, only limitation is the speed which they communicate.

Programming this req’s a specific DSL like CUDA or OpenCL. Includes a host (CPU) and kernel (GPU) program that can be very complex.

## SIMD Systems

SIMD = Single thread of Instructions; Multiple Data items. A SIMD system consists of an array of worker processors and a distinguished control processor. Each core does its own work & optionally returns info to instruction processor.

Programmed used data-parallel languages (Fortran, Fortran 95 etc). Lock-step.

## MIMD Systems

MIMD = Multiple threads of Instructions; Multiple Data items. Multiple cores doing their own thread of execution, and communicate in shared memory space or message passing.

### Shared Memory MIMD

SMPs — A single physical memory accessible to all processors.

NUMAs — A virtual single address space built on top of a set of distributed memory modules attached to the processors

### Distributed Memory MIMD

MPPs – The processors are tightly coupled with a custom

interconnection network.

Clusters — No special constraints. Scalability is unlimited, but harder to program.

## SMP Systems

SMP = Symmetric MultiProcessors

Commodity microprocessors connected to a single shared memory through

a high-speed interconnect, typically a bus or a crossbar.

- Symmetric — identical processors 
- Single physical address space and single I/O system 
- Hardware-supported cache coherence 

SMPs are heavily favored to run commercial applications, e.g. as database

or Internet servers.

## NUMA Systems

NUMA = Non-Uniform Memory Access

Most NUMA systems designed and built are cache-coherent NUMAs

(cc-NUMAs).

- The memory is physically distributed among different processors. 
- The system hardware and software maintains coherent caches, and create an illusion of a single address space to application users. 

They are more scalable than SMPs, but the overhead of maintaining cache-coherence can be high. All major vendors of computer systems are producing and selling cc-NUMA machines.

## MPP Systems

MPP = Massive Parallel Processing

An MPP system consists of a large number of nodes tightly-integrated by a very fast, custom interconnection network

MPPs have the scalability and locality advantages over SMPs.

Main Issues:

- Non-local data access is more expensive than on shared-memory systems 
- Programming is much more challenging 

## Cluster Systems

A cluster is a parallel computer system comprising an integrated collection of independent (“off-the-shelf”) nodes, each of which is a system in its own right, capable of independent operation.

## Message Passing Systems

- For MPPs or clusters with a flat interconnect: 
    - a host language (e.g. C or Fortran) + an explicit message-passing library (e.g. MPI) 

- For MPPs or clusters with a hierarchical interconnect: 
    - A combination of everything, e.g., a host language + a message-passing library + a share-memory library 

A common combination is C + MPI + OpenMP.

# Week 2 - Pthreads

## Slides
- [Shared](lecture/shared-2up.pdf)
- [Pthreads](lecture/pthreads-2up.pdf)

Pthreads = POSIX Threads

Every thread is either detached or joinable:

- Only a joinable thread can be synced 
- In most Pthreads implementations, all are joinable - not default 

## Shared Memory Systems

Systems that have:

- A moderate set of identical processors 
- A physical or virtual shared memory 

Usually used two ways:

1. Running multiple unrelated programs concurrently 
    1. OS feature 

2. Running a single program with multiple threads 
    1. How parallel programs are implement 

# Week 3 - Memory Consistency

## Slides
- [Consistency](lecture/consist-2up.pdf)
- [Data Parallelism](lecture/datapar-2up.pdf)

## Memory Consistency Problems

There are two memory-related consistency problems:

1. Cache coherence - multiple copies of the same data 
2. Memory consistency - consistent view of memory operations among processors 

## Cache Coherence

1. Invalidate - when there’s a new write, invalidate all other read copies of that data 
2. Update - when a new item is written, all other copies are updated 

### Hardware Support

1. Bus snoopy 
    1. Assume multiprocessors with private caches are placed on a shared bus 
    2. Each processor’s cache controller continuously snoops on the bus 
    3. Once a transaction is caught: 
        1. Invalidate the copy 
        2. Update the copy 

2. Directory-based 
    1. Use a directory to record all locations and states 
    2. If an item changes, update/ invalidate all the things 
    3. Works on machines w/ physically distributed memory/ scalable (supposedly) 

## Memory Consistency

Refers to the consistency of views on memory operations among processors.

### Operation Orders

1. Program - R/W ops defined by the source program 
2. Target code - R/W ops defined by the executable code 
3. Memory operation - R/W ops committed to memory 

Due to compiler optimizations, code order isn’t necessarily program order.

Due to hardware optimizations, commit order isn’t necessarily code order.

### Sequentially Consistent

A multiprocessor is sequentially consistent if the reads and writes by all processors appear to execute serially in a single global memory order that conforms to the program order in individual processors.

- All memory operations form a single global commit order and 
- The commit order conforms to the program order of each processor 

#### Constraints

1. Program order requirement 
    1. Memory ops of a processor must appear to become visible, to itself and others in program order 
    2. Commit order = code order = program order 

2. Write atomicity requirement 
    1. Each write happens atomically 
    2. All local views wrt. Write operation ordering are identical 

#### Preserving SC

1. Every process issues memory ops in program order 
2. After a write op is issued, the issuing process waits for the write to complete before going on 
3. After a read, the issuing proc waits for the read to complete, and for the returned value from a write is returned before going on (write atomicity) 

#### Implications

1. Compiler cannot change the order of memory operations 
2. Processor can’t use out-of-order processing techniques 

### Relaxing Requirements

Four kinds of program orders between memory ops (where the second can happen before the first):

1. Write-to-read 
2. Write-to-write 
3. Read-to-write 
4. Read-to-read 

#### Models

All usually implemented using hardware:

1. Total Store Ordering (TSO) 
    1. Relaxing write-to-read program order 

2. Processor Consistency (PC) 
    1. TSO w/o atomic writes 

3. Partial Store Ordering (PSO) 
    1. Relax write-to-read and write-to-write program orders 

Alternatives:

1. Weak Ordering (WO) 
    1. Relaxing all program orders on regular operations, requiring SC on sync’d operations 

2. Release Consistency (RC) 
    1. Similar to WO, except all sync’s are divided into acquires and releases (looks like pthread mutexes) 

# Week 4 - Data Parallel Programming

## Slides
- [Open MP](lecture/openmp-2up.pdf)

Fortran.

## Forall Statement Semantics

- Only assignment statements, where statements, and nested forall statements are allowed inside a forall statement. 
- All reads from a given assignment statement, in all iterations, must occur before any write to the left-hand side, in any iteration. 
- The writes of the left-hand side must occur before any reads in the following assignment statement. 

# Week 5 - OpenMP

## Slides
- [Open CL](lecture/opencl-2up.pdf)

API for compiler directives, library routines, and environment variables that can implement shared-memory parallelism.

Non-intrusive (ish). Explicitly allows for parallelism form specification.

## Overview

OpenMP is composed of:

1. Compiler directives (non-intrusive) 
2. Runtime libraries (intrusive) 
3. Environment variables (external) 

Execution model is fork-join.

## Parallel Regions

[http://jakascorner.com/blog/](http://jakascorner.com/blog/)

Specified by #pragma omp parallel. All code (w/ curlies) is then parallel.

Can set the number of threads (num_threads) and thread ids can be used to identify different thread.

### Tasks

[http://www.nersc.gov/users/software/programming-models/openmp/openmp-tasking/openmp-task-intro/](http://www.nersc.gov/users/software/programming-models/openmp/openmp-tasking/openmp-task-intro/)

[http://www.icl.utk.edu/~luszczek/teaching/courses/fall2016/cosc462/pdf/W45L1%20-%20OpenMP%20Tasks.pdf](http://www.icl.utk.edu/~luszczek/teaching/courses/fall2016/cosc462/pdf/W45L1%20-%20OpenMP%20Tasks.pdf)

### For loops

Can distribute a loop using the for directive.

### Sections

Explicitly specifies code for each individual thread to run.

[http://jakascorner.com/blog/2016/05/omp-sections.html](http://jakascorner.com/blog/2016/05/omp-sections.html)

### Reduction

Op:private specifies how to reduce a private variable.

[http://jakascorner.com/blog/2016/06/omp-for-reduction.html](http://jakascorner.com/blog/2016/06/omp-for-reduction.html)

### Synchronization

1. Critical - a block can only be executed by one thread at a time 
2. Barrier - sync’s all threads (expensive) 
3. Flush - force a consistent memory view for a thread 
4. Atomic - avoid conflicts when writing to memory 
5. Ordered - this block is executed in serial 

## Environment Variables

  

 

name

 

What do

 

OMP_SCHEDULE

 

Applies to loop-related directives w/ a schedule=runtime clause. Determines how iterations of the loop are scheduled

 

OMP_NUM_THREADS

 

Sets max number of threads

 

OMP_DYNAMIC

 

Enable/ disable dynamic adjustment of the number of threads available  for parallel regions

 

OMP_NESTED

 

Enable/ disable nested parallelism

  

# Week 6 - Chapel

## Slides

- [Chapel](lecture/chapel-2up.pdf)

Chapel.

## Good resources

[Learn Chapel in Y Minutes](resources/learn-chapel.html) - for super quick language stuff

[https://chapel-lang.org/tutorials.html](https://chapel-lang.org/tutorials.html) - tutorials

[https://chapel-lang.org/docs/](https://chapel-lang.org/docs/) - documentation
[https://chapel-lang.org/tutorials/SC10/](https://chapel-lang.org/tutorials/SC10/) - SC10 tutorials

# Week 7 - Message Passing Programming

## Slides

- [Open MPI](lecture/mpi-2up.pdf)
- [Message Passing](lecture/msgpass-2up.pdf)

## Message Passing

  

Def: A large number of compute nodes that are connected in a network that do some work by communicating data and collecting/ distributing computation.

  

Programs need to partition and distribute computation in this kind of parallel programming model.

  

### Approaches

  

* Explicit Message Passing (a C, Open MPI etc)

* Implicit Message Passing (Chapel, CAF, X10) marked as experimental

  

### Explicit Message Passing

  

theory: every compute node runs its own independent program.

reality: more like one of the following:

  

* SPMD style (same program, multiple data) where each process runs the same program w/ control flow customizing the work done by each process

* Master-slave style (one copy of master program, many copies of slave program)

  

#### Partition and Mapping

  

Which do you partition first: data or computation? How do you map these partitions?

  

##### Approach 1 - Task-based Parallelism

  

Decompose the computation task into partitions, partition data later.

  

Not suitable for large-scale message-passing systems. Why? Each task might need access to the same data repeatedly, which implies a lot of copying of data. Unnecessary.

  

##### Approach 2 - Data-based Parallelism

  

Decompose the data into disjoint partitions, and have each node perform computations on those partitions.

  

Useful for scientific applications.

  

"Owner computes" rule - every data item has an owner, and the owner is responsible for computation/ updating of that partition

  

#### Data Mapping Strategies

  

##### Block

  

Make blocks based on the number of processes. Clean, naive. We need to know the number, however, and that can't always be guaranteed.

  

##### Cyclic

  

Partition the data into small blocks, and have each process work on each partition in round robin.

  

##### Which to select?

  

Needs to be based on:

* Communication patterns

- Different mappings imply different communication patterns

* Communication amount

- Different mappings affect the ratio between interior/ border data, which means different amounts of data need to be communicated

* Data alignment

- Multiple data objects need to be mapped in a coordinated way

* Load balancing

- Computation may not always be uniform across a data domain

  

#### Communication

  

Passing messages imply that we figure out:

* When to communicate (blocking/ non-blocking)

* Where to communicate (global memory bus, process-process buffer)

* How to communicate (communication routines)

  

##### Send/ Recieve Primitives

  

Send and recieve primitives allow for a basic communication method, for point-point (assume blocking) communication.

  

Send:

* Issue send

* Copy data to system buffer

* Wait for reciever

* Send to reciever

  

Recieve:

* Issue recieve

* Check for message in system buffer

* Wait for a message

* Copy the message if/ when it's there to the user space

  

###### Blocking/ Non-blocking

  

Blocking implies that we can safely reuse/ modify contents of buffers that we use to communicate, since only one routine has access to it at the same time.

Non-blocking doesn't give us that implication, and depending on the message-passing routine we decide on we can only make a few kind of assumptions before/ after a call.

  

##### Collective Communication

  

Patterns for collective communication:

1. One-to-many - sending data from one node to many nodes

a. Broadcast - one-&gt;all

b. Multicast - one-&gt;subset

c. Scatter - one-&gt;some other

2. Many-to-one - sending data from many nodes to one

a. Reduce - many to one

b. Gather - many (subset) to one

3. Many-to-many - concurrent, disjoint send-recieve patterns

a. This one had fun shapes

  

##### One-sided Communication - RMA (Remote Memory Access)

  

One process specifies all communication patterns, for send and recieve sides.

  

There are new issues, thought: Access conflicts, memory consistency, operation granularity. Its

semantics can be tricky to define.

  

## MPI

What is it?

A message-passing library specification for explicitly programming message-passing systems.

(Sinful, in other words).

### Features

  

* Point-point communication
* Collective communication
* One-sided communication
* Others
	- Message security, thread safety
	- Profiling hooks, error control

  

### Parts of program

  

Six functions:

1. MPI_Init - start
1. MPI_Finalize - exit
1. MPI_Comm_size - number of procs
1. MPI_Comm_rank - which proc?
1. MPI_Send - send some data
1. MPI_Recv - receive some data

  

# Week 8 - Sorting Algorithms

## Slides
- [Sorting](lecture/sorting-2up.pdf)

## Sorting Algorithms

- Based on data location
	- Internal Sort
		- source data and final result are both stored in a processor's main memory
	- External Sort
		- source data and final result are both stored on disk
		- data is brought into memory for processing, one portion at a time
- Based on data information
	- Comparison-based sorting
		- without assuming any knowledge of data distribution
		- well-known lower bound of $ln(n!)$
	- Distribution-based sorting
		- with knowledge of data distribution
		- upper bound of $O(n \log n)$.
		- bucket sort
	- Adaptive sorting
		- without knowledge of data distribution
		- try to guess patterns in data and use that to sort
		- sample sort

### Bubble Sort

[Lecture](lecture/sorting-2up.pdf#page=2)

1. Scan the array $n - 1$ times. Reduce the range by one per scan.
1. In each scan, compare and swap if they're mis-ordered

*Complexity*: $O(n^2)$.

#### Parallel

Improvement on bubble sort.

For each bubbling action, if the swapping elements are disjoint so the bubbling
won't overlap.

*Complexity*: $2n$.

#### Odd-Even Sort

'Improvement' on parallel bubble sort.

1. On each phase, consider all even or odd elements alternating
1. Compare and swap each element if they're misordered.

#### Shellsort

Improvement on odd-even sort.

1. Phase 1 - $\log n$ steps
	1. Pair processes by 'distances'
	1. Distances halve each step
1. Phase 2 - Run the odd-even sort

*Complexity*: $\leq n$ steps.

### Selection Sort

[Lecture](lecture/sorting-2up.pdf#page=4)

1. Scan the array $n-1$ times, with range reduced by one per scan
1. In each scan, the largest element in the range is found and swapped to the end of the range

#### Parallel

* Assume finding `max` can be done in $O(\log n)$ steps.
* Algorithm still needs $O(n)$ iterations
* Total of $O(n \log n)$ time for a parallel version

*Conclusion*: a better sequential algorithm doesn't necessarily translate to better parallel program.

### Quicksort

[Lecture](lecture/sorting-2up.pdf#page=5)

Sorting is done in-place:
1. Select a pivot value
	1. Use pivot to divide array into two sections
	1. In one section, all elements are smaller than the pivot
	1. In the other, all elements are larger
1. Recursively quicksort the two sections

*Complexity*: $O(n \log n)$ on average, $O(n^2)$ in the worst case

Simple improvement: use sampling to select the pivot:
```
pivot = average(a[low], a[(low+high)/2], array[high]);
```

#### Parallel Quicksort

Assign the recursive calls to processes.

*Complexity*: $O(\log n)$ with $n$ processes (in the average case)
*Drawback*: At the beginning, only one process is active

#### Hyper-Quicksort

Load-balanced version of quicksort.

[Look at the slides for an explanation.](lecture/sorting-2up.pdf#page=6)

### Bucket Sort

Logical extension to quicksort.

Partition array into $>2$ sections.

$m$ is number of sections/ buckets, $n$ is the number of items.

*Complexity*: $O(n + n \log(\frac{n}{m}))$ - on average

Linear if sorting a random distribution of integers, and $m = n$ (roughly)

### Parallel Bucket Sort

If you know the distribution of the data, you can
divide the work into sections on the known interval
and sort that in parallel. Only works if you actually
know this magic number beforehand.

### Sample Sort

Attempts to merge bucket sort and hyper-quicksort.

[In lecture](lecture/sorting-2up.pdf#page=9).

Processes sort little bits of data, and send random
samplings to a master processor that sorts the samples,
and the sorted broadcast tells each processes where to send
different partitions.

Each process then merges those partitions.

### Mergesort

Works like the inverse of parallel quicksort.

*Complexity*: $O(\log n)$ with $n$ processes - average case.

### Bitonic Mergesort

[A bitonic sequence is](lecture/sorting-2up.pdf#page=11):

> two sorted lists concatenated together, with
or without a circular shift

or:

> A bitonic sequence is a sequence $a_0, … , a_{n−1}$ such that
$a_0 ≤ … ≤ a_k ≥ … ≥ a_{n−1}$ for some $k$, $0 ≤ k < n$, or a circular
shift of such a sequence.

or:

> A Bitonic Sequence is a sequence of numbers which is first strictly increasing then after a point strictly decreasing.

A key property is that two bitonic sequences cross each
other at most once.

#### Bitonic Sort Algorithm

Formally.

1. Form $\dfrac{n}{2}$ trivial bitonic sequences
1. Sort the bitonic sequences
1. Continue until the list is sorted

*Complexity*: $O(\log^2 n)$.

### External Sorting

Large data sets imply two passes over the data:
1. scan
2. write sorted

Algorithms tend to:
1. First write half-processed temp files
1. Combine the temps into the sorted result

Categories:

- Distribution-Based sorting
	- First stage partitions data into disjoin buckets
	- Each bucket is independently sorted
- Merge-based sorting
	- First stage partitions data into chunks of approx. equivalent size
	- Sort the runs in main memory and write individual runs to disk
	- Second stage merges the runs in main memory and writes sorted to disk

Then a bunch of case studies.

# Week 9 - PGAS Languages and Performance

## Slides
- [PGAS](lecture/pgas-2up.pdf)
- [Performance](lecture/perf-2up.pdf)

## PGAS

> PGAS = Partitioned Global Address Space Languages

Supposedly for high-level message-passing programming.

Languages:

- Co-Array Fortran
- [Unified Parallel C](lecture/pgas-2up.pdf#page=3)
- X10
- [Chapel](lecture/pgas-2up.pdf#page=9)
