# Response to Assignment 1
## Prompt
Write a short (one-page) summary (in pdf or plain text) covering your experience with this assignment.
What issues did you encounter?, How did you resolve them? What is your thread termination approach?
What can you say about the work distribution of your program? etc.

## Response

### Undergrad Response
The producer-consumer pattern was somewhat familiar to me already, because of work with Go that I had done previously.
I knew that the best way (that I knew how) would be to make something that mimicked channels.
I knew that having a `NULL` queue at some point (analagous to `close`-ing a channel) would cause issues, so I elected to use a `volatile` bool for the time being.
It's not been reviewed as the best thing, but it worked in testing from 1-1000 consumers at a time.
The best way to replace this mechanism would be some kind of `bool` with a lock, which would be queried by `consume()` if a task is `NULL`, and would indicate that the producer was done or not.


One issue I encountered early on was assuming that the queue implementation provided was thread safe, which of course caused segmentation faults - this was pretty quickly resolved due to experience with `pthread`-like threads in CS333.

Another issue I came across was the lack of task distribution - the producer would stop being able to add tasks because one consumer was hogging control. This was fixed with `pthread_yield()` calls in the right spots, along with some calls in `consume()`.

### Grad Response
The approach I took for this portion was to extend the existing concepts in the undergrad response and make them actually do important work.

One major challenge was synchronizing the results feed for the primes at the end. I decided that it would be better for each thread to pool as many results as they could before handing over control to some collection routine. To do so, I decided to have each consumer have a results spool where they would deposit results that would later be collected into the result array as the lock is left unused.

One limitation while testing the programs was that if the array size was large enough, the program would segfault with `pthread_join.c: No such file or directory`. Stepping through the program with GDB yielded a segmentation fault that wasn't as specific as I was hoping.
The issue ended up being the way work was divided in the `consume_args->raw` portion of `consume()`. Basically, the consuming thread would index out of `raw` and end up segfaulting there.


### General notes
An added note, I didn't catch `run1` or `run2` until I had already created the two makefile targets for running the tests. I'm going to submit the samples I wrote, but there are existing targets at my class repo ([link](https://gitlab.com/benjspriggs/cs415p)) for the two provided scripts.

Using those scripts helped catch a `prodcons-pthd 0` error, where the producer would just hang, waiting for consumers to join.

#### Post turn-in

So I apparently was supposed to use conditional variables, thankfully re-working both of my responses didn't take much time.

There was an issue where there were languishing consumer threads waiting for the `q_not_full` signal to be able to be reaped, which was found after a rather stressful night of debugging.
This processes illuminated the sometimes arbitrary and honestly quite capricious nature of parallel debugging.
