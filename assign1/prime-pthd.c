#define _GNU_SOURCE
// includes
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sched.h>
#include <math.h>

#include "task-queue.h"

// debug printing, print helpers
#define puts(f) printf(f "\n")
#define print(f_, ...) printf((f_ "\n"), __VA_ARGS__)
#ifdef DEBUG
#define dprint(f_, ...) print(f_, __VA_ARGS__)
#else
#define dprint(...)
#endif

// constants
// globals
const int PARTITION = 10;
volatile bool done_producing = false;

int get_count_elements(int* head, int* tail) {
  if (head == tail)
    return 0;

  if (tail < head)
    return (head - tail) / sizeof(int);
  else
    return (tail - head) / sizeof(int);
}

// included from prime.c
// Return 1 if k is a prime 
// 
int isPrime(int k) {
  int limit = (int) sqrt((double) k);
  if (k == 1) 
    return 0;
  for (int i = 2; i <= limit; i++)
    if ((k % i) == 0)
      return 0;
  return 1;
}

// Swap two array elements 
// 
void swap(int *array, int i, int j) {
  if (i == j) return;
  int tmp = array[i];
  array[i] = array[j];
  array[j] = tmp;
}

// Initialize array.
// - first generate [1,2,...,ARRAYSIZE]
// - then perform a random permutation
//
int *init_array(int N) {
  int *array = (int *) malloc(sizeof(int) * N);
  int i, j;
  for (i = 0; i < N; i++) {
    array[i] = i + 1;
  }
  srand(time(NULL));
  for (i = 0; i < N; i++) {
    j = (rand() * 1. / RAND_MAX) * (N - 1);
    swap(array, i, j);
  }
  return array;
}

// end includes

// sync_t
typedef struct {
  pthread_mutex_t* q_mutex;
  pthread_cond_t* q_not_empty;
  pthread_cond_t* q_not_full;
} sync_t;

// produce_arg_t
typedef struct {
  queue_t* task_queue;
  sync_t s;
  int N;
  int* raw; // raw number array to be sorted into primes
} produce_arg_t;

// produce the task queue
void produce(void* ctx) {
  produce_arg_t* p = (produce_arg_t*) ctx;

  print("Master starting on core %d", sched_getcpu());

  int num_partitions = p->N / PARTITION;

  dprint("num_partitions = %d", num_partitions);

  // send tasked partitions to the task queue
  for (int i = 0; i < num_partitions; ++i) {
    task_t* t = create_task(i);

    pthread_mutex_lock(p->s.q_mutex);

    dprint("Master attempting to add task %d", i);
    while(add_task(p->task_queue, t) == 0) {
      pthread_cond_wait(p->s.q_not_full, p->s.q_mutex);
      pthread_mutex_unlock(p->s.q_mutex);
      dprint("Master yielding control, failed add for %d", i);
      pthread_mutex_lock(p->s.q_mutex);
    }
    dprint("Master successfully added task %d", i);
    pthread_cond_broadcast(p->s.q_not_empty);

    pthread_mutex_unlock(p->s.q_mutex);
  }

  done_producing = true;

  puts("Master ending");
}

// consume_arg_t
typedef struct {
  queue_t* task_queue;
  sync_t s;
  int* raw; // raw number array to be sorted into primes
  void (*write_result)(int); // writes
  int* results;
  int id;
} consume_arg_t;

// Submits primes to the primes array
// from a result_q for the consuming thread.
// Note: there isn't an explicit locking mechanism
// to the result_q, since we are assuming that
// each consumer thread has one and only one result queue.
void submit_result(pthread_mutex_t* lock, queue_t* result_q, void (*write_result)(int)) {
  if (write_result == NULL) {
    return;
  }

  // to try locking, then if it's open add
  // o/w just continue right along
  if (pthread_mutex_trylock(lock) == 0) {
    dprint("Got lock in submit_result, %p", write_result);

    task_t* t;
    while((t = remove_task(result_q)) != NULL) {
      dprint("Moving task w/ val %d to write_result", t->val);

      (*write_result)(t->val);
    }

    pthread_mutex_unlock(lock);
  }
}

// Consume and compute a partition of primes
// taken from the task queue.
void consume(void* ctx) {
  consume_arg_t* c = (consume_arg_t*)ctx;

  print("Worker[%d] started on core %d", c->id, sched_getcpu());
  queue_t* result_queue = init_queue(0);

  while (true) {
    // submit_result will deal with locking for us
    submit_result(c->s.q_mutex, result_queue, c->write_result);

    pthread_mutex_lock(c->s.q_mutex);
    task_t* t = remove_task(c->task_queue);

    // ensure we get tasks properly
    if (t == NULL) {
      dprint("Worker[%d] found a null task", c->id);
      if (done_producing) {
        dprint("Worker[%d] saw that we're done producing and is shutting down", c->id);
        pthread_mutex_unlock(c->s.q_mutex);
        break;
      }

      dprint("Worker[%d] saw that we're not done producing", c->id);
      pthread_cond_wait(c->s.q_not_empty, c->s.q_mutex);
      pthread_mutex_unlock(c->s.q_mutex);
      dprint("Worker[%d] is politely releasing the lock and yielding control", c->id);
      continue;
    }

    dprint("Worker[%d] signaling that the queue is not full", c->id);
    pthread_cond_signal(c->s.q_not_full);

    // release, since we don't need to sync with the main task queue
    pthread_mutex_unlock(c->s.q_mutex);

    dprint("Worker[%d] took task %d", c->id, t->val);

    // do the work of finding primes here
    int start = t->val * PARTITION,
        end = (start + PARTITION) - 1;
    for (int i = start; i < end; ++i) {
      if (isPrime(c->raw[i])) {
        task_t* rt = create_task(c->raw[i]);
        dprint("Found prime %d at index %d", c->raw[i], i);
        // no need for lock, we're the only one that owns it
        while(add_task(result_queue, rt) == 0);
      }
    }

    // NOW, I know there's not a synchronization mechanism here.
    // Since we know that only one thread can ever touch the result
    // array, we don't need to check write/reads since it will be
    // in-step with our thread.
    ++c->results[c->id];
  }

  print("Worker[%d] ending", c->id);
}

int main(int argc, char** argv){
  // validate arguments
  if (argc > 3 || argc < 2) {
    print("Usage : %s <size-of-array> [<num-threads>]", argv[0]);
    return -1;
  }

  // read arguments, set default
  int N = atoi(argv[1]), num_cons = 1;

  // check partition size
  if (N % PARTITION != 0) {
    print("<size-of-array> must be divisible by %d", PARTITION);
    return -1;
  }

  if (argc == 3) {
    num_cons = atoi(argv[2]); 

    if (num_cons < 0) {
      return -1;
    }
  }

  print("Main: started ... array[%d] with %d thread(s)", N, num_cons);

  // infinte task queue
  queue_t* task_queue = init_queue(0);
  // task queue mutex
  pthread_mutex_t q_mutex;
  pthread_mutex_init(&q_mutex, NULL);

  pthread_cond_t q_not_empty = PTHREAD_COND_INITIALIZER;
  pthread_cond_t q_not_full = PTHREAD_COND_INITIALIZER;

  sync_t s = { &q_mutex, &q_not_empty, &q_not_full };

  // used to store 'hit' results
  int* results = malloc(num_cons * sizeof(int));
  // array of random numbers
  int* raw = init_array(N);
  // used to store resulting primes
  int* primes = malloc(N * sizeof(int));
  // points to the next unused spot in the primes array
  int primes_tail = 0;
  // consumer threads
  pthread_t* consumers = malloc(num_cons * sizeof(pthread_t));

  // do the darn thing
  pthread_t producer;
  produce_arg_t* produce_args = malloc(sizeof(produce_arg_t));
  {
    produce_arg_t p = { task_queue, s, N, raw };
    *produce_args = p;
  }

  int err = pthread_create(&producer, NULL, (void*) produce, (void*) produce_args);

  // handle errors
  if (err) {
    pthread_join(producer, NULL);

    print("Spawning producer thread caused err %d, exiting", err);
    return err;
  }

  // create consumers
  consume_arg_t* consume_args;

  // how we're going to update the result array
  // this code is going to be called in a thread-safe context
  // we just need a portable callback
  void write_result(int submit) {
    primes[primes_tail] = submit;
    ++primes_tail;
  }

  for (int i = 0; i < num_cons; ++i) {
    consume_args = malloc(sizeof(consume_arg_t));
    consume_args->task_queue = task_queue;
    consume_args->s = s;
    consume_args->raw = raw;
    consume_args->write_result = write_result;
    consume_args->results = results;
    consume_args->id = i;

    int err = pthread_create(&consumers[i], NULL, (void*) consume, (void*) consume_args);

    if (err) {
      exit(1);
    }
  }

  // re-join
  pthread_join(producer, NULL);

  pthread_cond_broadcast(&q_not_full);
  pthread_cond_broadcast(&q_not_empty);

  for (int i = 0; i < num_cons; ++i) {
    pthread_join(consumers[i], NULL);
  }

  puts("Main: ... all threads have joined");

  // final printing
  print("Found %d primes in array[%d]", primes_tail, N);

  for (int i = 0; i < primes_tail; ++i) {
    printf("%d, ", primes[i]);
  }

  puts("");

  for (int i = 0; i < num_cons; ++i) {
    if (i > 0 && i % 4 == 0) {
      puts("");
    }

    printf("W[%2d]: %d,\t", i, results[i]);
  }


  puts("");

  print("Total tasks across threads: %d", N / PARTITION);

  return 0;
}
