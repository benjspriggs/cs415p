#define _GNU_SOURCE
// includes
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sched.h>

#include "task-queue.h"

// debug printing, print helpers
#define puts(f) printf(f "\n")
#define print(f_, ...) printf((f_ "\n"), __VA_ARGS__)
#ifdef DEBUG
#define dprint(f_, ...) print(f_, __VA_ARGS__)
#else
#define dprint(...)
#endif

volatile bool done_producing = false;

const int QUEUE_CAPACITY = 20;
const int TASK_MAX = 100;

// sync_t
typedef struct {
  pthread_mutex_t* q_mutex;
  pthread_cond_t* q_not_empty;
  pthread_cond_t* q_not_full;
} sync_t;

typedef struct {
  queue_t* task_queue;
  sync_t s;
  int* results;
  int id;
} consume_arg_t;

// consumes on the queue until it closes
void consume(void* ctx) {
  consume_arg_t* c = (consume_arg_t*)ctx;
  print("Consumer[%2d] starting on core %d", c->id, sched_getcpu());

  while (true) {
    pthread_mutex_lock(c->s.q_mutex);
    task_t* t = remove_task(c->task_queue);

    pthread_cond_signal(c->s.q_not_full);

    if (t == NULL) {
      dprint("Consumer[%2d] found a null task", c->id);
      if (done_producing) {
        dprint("Consumer[%2d] saw that we're done producing and is shutting down", c->id);
        pthread_mutex_unlock(c->s.q_mutex);
        break;
      }
      else {
        dprint("Consumer[%2d] saw that we're not done producing", c->id);
        dprint("Consumer[%2d] is waiting for queue to not be empty", c->id);
        pthread_cond_wait(c->s.q_not_empty, c->s.q_mutex);
        dprint("Consumer[%2d] is politely releasing the lock", c->id);
        pthread_mutex_unlock(c->s.q_mutex);
        continue;
      }
    }
    pthread_mutex_unlock(c->s.q_mutex);

    dprint("Consumer[%2d] took task %d", c->id, t->val);

    // NOW, I know there's not a synchronization mechanism here.
    // Since we know that only one thread can ever touch the result
    // array, we don't need to check write/reads since it will be
    // in-step with our thread.
    ++c->results[c->id];
  }

  print("Conusmer[%2d] ending", c->id);
}

typedef struct {
  queue_t* task_queue;
  sync_t s;
} produce_arg_t;

// producer routines
void produce(void* cxt) {
  produce_arg_t* p = (produce_arg_t*) cxt;

  print("Producer starting on core %d", sched_getcpu());

  for (int i = 1; i <= TASK_MAX; ++i) {
    task_t* nt = create_task(i);

    pthread_mutex_lock(p->s.q_mutex);
    // add_task returns the queue's new length
    while(add_task(p->task_queue, nt) == 0) {
      pthread_cond_wait(p->s.q_not_full, p->s.q_mutex);
    }

    dprint("Producer creating task %d", i);
    pthread_cond_broadcast(p->s.q_not_empty);
    pthread_mutex_unlock(p->s.q_mutex);
  }

  done_producing = true;

  puts("Producer ending");
}

// main
int main(int argc, char** argv) {
  if (argc > 2 || argc < 0) {
    return -1;
  }

  // initialize queue, lock
  int num_cons = 10;
  queue_t* task_queue = init_queue(QUEUE_CAPACITY);
  pthread_mutex_t q_mutex;
  pthread_mutex_init(&q_mutex, NULL);
  pthread_cond_t q_not_empty = PTHREAD_COND_INITIALIZER;
  pthread_cond_t q_not_full = PTHREAD_COND_INITIALIZER;

  sync_t s = {&q_mutex, &q_not_empty, &q_not_full};

  if (argc == 2) {
    num_cons = atoi(argv[1]); 

    if (num_cons < 1) {
      return -1;
    }
  }


  // do the thing
  puts("Main: started ...");

  int* results = malloc(num_cons * sizeof(int));

  pthread_t* consumer_threads = malloc(num_cons * sizeof(task_t));
  pthread_t producer;

  // create producer
  produce_arg_t* produce_args = malloc(sizeof(produce_arg_t));
  {
    produce_arg_t p = { task_queue, s };
    *produce_args = p;
  }
  int err = pthread_create(&producer, NULL, (void*) produce, (void*) produce_args);

  // handle errors
  if (err) {
    pthread_join(producer, NULL);

    print("Spawning producer thread caused err %d, exiting", err);
    return err;
  }

  // create consumers
  consume_arg_t* consume_args;
  for (int i = 0; i < num_cons; ++i) {
    consume_args = malloc(sizeof(consume_arg_t));
    consume_arg_t c = { task_queue, s, results, i };
    *consume_args = c;
    int err = pthread_create(&consumer_threads[i], NULL, (void*) consume, (void*) consume_args);

    if (err) {
      break;
    }
  }

  // re-join
  pthread_join(producer, NULL);

  pthread_cond_broadcast(&q_not_full);
  pthread_cond_broadcast(&q_not_empty);

  for (int i = 0; i < num_cons; ++i) {
    pthread_join(consumer_threads[i], NULL);
  }

  puts("Main: ... all threads have joined!");

  // print results
  print("Total tasks across threads: %d", TASK_MAX);

  for (int i = 0; i < num_cons; ++i) {
    if (i > 0 && i % 4 == 0) {
      puts("");
    }

    printf("C[%2d]: %d,\t", i, results[i]);
  }

  puts("");

  return 0;
}
