#!/bin/bash
# verify.sh <program>
array=()

IFS=' ' read -r -a array <<< $2

for n in "${array[@]}"; do
	for file in `ls data*k`; do
		sorted="sorted-$1-$file.txt"
		mpirun -n $n ./$1 $file $sorted
		./verify $sorted
		echo Verify the results...
		sleep 1
		rm -f $sorted
	done
done
