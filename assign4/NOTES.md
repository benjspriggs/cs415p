# Notes
## Benjamin Spriggs

### Verification

Run the following two targets with `make`:
```make
bsort-mpi-test: verify bsort-mpi
	bash verify.sh bsort-mpi '1 2 4 8 16'

bsort2-mpi-test: verify bsort2-mpi
	bash verify.sh bsort2-mpi '1 2 4 7 8 16 32'
```

### Issues

There were some memory issues, especially setting up the `Alltoall` communication. Eventually, after a lot of print statements and slow debugging, I found that a lot of my issues came from buffer size mismatch.
I ended up more or less implementing the pack and unpack for my program specifically, since the custom type interface for MPI leaves a *lot* to be desired.

One issue that was particularly frustrating was the input reading and output reading. Somehow, my file sizes for sorted and unsorted inputs were different - they should have been the same.
The issue came from not reading the correct number of bytes into the file, and not properly writing the right byte offset. I was writing the number of ints!
