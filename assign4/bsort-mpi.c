#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <mpi.h>

#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))

// Specify data value range by bit-width 
//
#define DATABITS 13     // assume data are 13-bit integers: [0,8191] 

// Swap two array elements 
//
void swap(int *array, int i, int j) {
  if (i == j) return;
  int tmp = array[i];
  array[i] = array[j];
  array[j] = tmp;
}

// Bubble sort for the base cases
//
void bubblesort(int *array, int low, int high) {
  if (low >= high) return;
  for (int i = low; i <= high; i++)
    for (int j = i+1; j <= high; j++) 
      if (array[i] > array[j])
  swap(array, i, j);
}

// Initialize array
//
void init_array(int *a, int n) {
  srand(time(NULL));
  for (int i = 0; i < n; i++)
    a[i] = rand() % 8192;
}

// Verify that array elements are sorted
//
void verify_array(int *array, int N) {
  for (int i = 0; i < N-1; i++) {
    if (array[i]>array[i+1]) {
      printf("FAILED: array[%d]=%d, array[%d]=%d\n", 
    i, array[i], i+1, array[i+1]);
      return;
    }
  }
  printf("Result verified!\n");
}

// Print array
//
void print_array(int *array, int N) {
  for (int i = 0; i < N; i++) {
    printf("%4d ", array[i]);
    if (i % 12 == 11)
      printf("\n");
  }
  printf("\n");
}

// Find bucket idx for an int value x of b bits
//
int bktidx(int b, int x, int B) {
  return x >> (b - (int)log2(B));
}

// BEGIN USER DEFINED FUNCTIONS
void usage(int err) {
  printf("./bsort-mpi <infile> <outfile>\n");
  MPI_Finalize();
  exit(err);
}

// defines a bucket
// that has some data, and a
// hard ceiling that MUST NEVER BE HIT
typedef struct {
  int max_bucket_size;
  int bucket_size;
  int bucket_count;
  int* data;
} bucket_t;

void push(bucket_t* b, int val) {
  if (b->bucket_count == b->bucket_size) {
    // added due to an issue with some 
    // buckets being overflown w/ values
    printf("***BUCKET*** bucket had to expand to get this next value, doubling size %d to %d\n",
  b->bucket_size,
  2 * b->bucket_size);
    b->bucket_size *= 2;

    // another level of validation to ensure
    // we don't overflow the 2N/P limit
    if (b->bucket_size > b->max_bucket_size) {
      printf("***FATAL*** bucket too big, %d when max was %d\n",
    b->bucket_size,
    b->max_bucket_size);
      exit(1);
    }

    // reallocate the data
    b->data = realloc(b->data, b->bucket_size * sizeof(int));
  }

  // validate our bucket size is under the limit
  if (b->bucket_count == b->max_bucket_size) {
    printf("***FATAL*** bucket exceeded maximum size, max size %d w/ %d\n", 
  b->bucket_count, 
  b->bucket_size);
    MPI_Finalize();
    exit(1);
  }

  b->data[b->bucket_count++] = val;
}

// context for the whole program
typedef struct {
  int rank;
  int size;         // == P
  int bucket_size;  // num of elems in each bucket
  MPI_Offset N;     // number of data items
  int chunk_size;   // == N/P
  char* infile;     // filename for input
  char* outfile;    // filename for ouptut
  char hostname[MPI_MAX_PROCESSOR_NAME];   // hostname
  int* buffer;      // buffer of elems read from file - has chunk_size num elems
  bucket_t* buckets;
} context_t;

// free alloc'd elements in the context
void cleanup(context_t* c) {
  if (c == NULL)
    return;

  free(c->buffer);

  for (int i = 0; i < c->size; ++i) {
    free(c->buckets[i].data);
  }

  free(c->buckets);
};

// print a tagged message to stdout
void print_tagged(context_t* c, char* msg) {
  printf("[%s:bsort(%3d/%3d)]:\t%s\n", 
      c->hostname, c->rank, c->size, msg);
}

// print a formatted and tagged message to stdout
void print_tagged_format(context_t* c, char* fmt, ...) {
  printf("[%s:bsort(%3d/%3d)]:\t",
      c->hostname, c->rank, c->size);
  va_list args;
  va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  printf("\n");
}

// print a nicely formatted stage message
void stage(context_t* c, char* stage) {
  print_tagged_format(c, "** STAGE: %s", stage);
}

// initializes the context,
// assuming a validated argv
context_t* init_context(char** argv){
  context_t* c = calloc(1, sizeof(context_t));

  c->infile = argv[1];
  c->outfile = argv[2];

  {
    int len = 0;

    MPI_Get_processor_name(c->hostname, &len);
  }

  // MPI Init
  int rank, size; 
  MPI_Comm_rank(MPI_COMM_WORLD, &c->rank);  
  MPI_Comm_size(MPI_COMM_WORLD, &c->size);  

  // Data Count
  print_tagged(c, "getting file size");
  MPI_File infile;

  MPI_File_open(MPI_COMM_WORLD, c->infile,
      MPI_MODE_RDONLY, MPI_INFO_NULL, &infile);

  MPI_File_get_size(infile, &(c->N));

  print_tagged_format(c, "read %ld byte infile", c->N);

  c->N /= sizeof(int);

  print_tagged_format(c, "read %ld ints", c->N);

  MPI_File_close(&infile);

  c->chunk_size = c->N / c->size;

  c->bucket_size = MAX(32, 2* (c->N / (c->size * c->size)));

  return c;
}

// return #count buckets with #slots each
bucket_t* new_buckets(int max_bucket_size, int count, int slots) {
  bucket_t* buckets = calloc(count, sizeof(bucket_t));

  for (int i = 0; i < count; ++i) {
    buckets[i].bucket_size = slots;
    buckets[i].data = calloc(slots, sizeof(int));
    buckets[i].bucket_count = 0;
    buckets[i].max_bucket_size = max_bucket_size;
  }

  return buckets;
}

// return the right amount of buckets for a context
// (ensure we don't overflow the 2N/P barrier
bucket_t* buckets_for(context_t* c) {
  return new_buckets(2 * (c->N/ c->size) , c->size, c->bucket_size);
}

// Input
void input_step(context_t* c) {
  stage(c, "reading data from file");

  // allocate buffer of N/P elems
  c->buffer = calloc(c->chunk_size, sizeof(int));

  int offset = c->rank * c->chunk_size;

  MPI_File infile;

  MPI_File_open(MPI_COMM_WORLD, c->infile,
      MPI_MODE_RDWR, MPI_INFO_NULL, &infile);

  // seek to our offset
  MPI_File_seek(infile, offset, MPI_SEEK_SET);
  // read chunk_size elems into buffer
  MPI_File_read(infile, c->buffer, c->chunk_size, MPI_INT, MPI_STATUS_IGNORE);

  // close the file
  MPI_File_close(&infile);
}

// Buckets
// parition buckets and distribute data
void data_step(context_t *c) {
  print_tagged(c, "creating buckets");
  c->buckets = buckets_for(c);

  // Distribution
  print_tagged(c, "distributing buckets");

  for (int i = 0; i < c->chunk_size; ++i) {
    int index = bktidx(DATABITS, c->buffer[i], c->size);
    push(&c->buckets[index], c->buffer[i]);
  }
}

// Shuffle Buckets
void shuffle_step(context_t* c) {
  stage(c, "shuffle step");

  // first we need the sizes of the buckets
  int* bucket_counts = calloc(c->size, sizeof(int));
  int* my_bucket_counts = calloc(c->size, sizeof(int));
  int* bucket_sizes = calloc(c->size, sizeof(int));
  int* my_bucket_sizes = calloc(c->size, sizeof(int));

  // initialize bucket counts, sizes for recieve
  // and initialize our own counts and sizes
  for (int i = 0; i < c->size; ++i) {
    bucket_counts[i] = 0;
    bucket_sizes[i] = 0;
    my_bucket_counts[i] = c->buckets[i].bucket_count;
    my_bucket_sizes[i] = c->buckets[i].bucket_size;
  }

  bucket_t* my_bucket = &c->buckets[c->rank];


  // send the relevant info to other processes
  MPI_Alltoall(my_bucket_counts, 1, MPI_INT,
      bucket_counts, 1, MPI_INT,
      MPI_COMM_WORLD);

  MPI_Alltoall(my_bucket_sizes, 1, MPI_INT,
      bucket_sizes, 1, MPI_INT,
      MPI_COMM_WORLD);

  // now we setup sending and recieving the buckets
  // displacements for each bucket
  int* sdisplacements = calloc(c->size, sizeof(int));
  int* rdisplacements = calloc(c->size, sizeof(int));
  int* send_counts = calloc(c->size, sizeof(int));

  // send and recieve buffers
  int *sends = calloc(c->N, sizeof(int));
  int *recieve = calloc(c->N, sizeof(int));

  // these are counts that we'll use to calculate the displacements
  // for the send and the recieve buffers
  MPI_Offset scount = 0, rcount = 0;

  print_tagged_format(c, "expecting at most %ld elems (N/P == chunk_size)", c->chunk_size);

  for (int i = 0; i < c->size; ++i) {
    // print_tagged_format(c, "packing send bucket %d at sends[%d]", i, scount);
    memmove(sends + scount,
  c->buckets[i].data, 
  my_bucket_counts[i] * sizeof(int));

    // print_tagged_format(c, "packing recieve proc(%d)'s bucket %d at recieve[%d]", i, c->rank, rcount);

    // calculate the displacements
    sdisplacements[i] = scount;
    rdisplacements[i] = rcount;

    // print_tagged_format(c, "sending %d items to proc %d", my_bucket_counts[i], i);
    // print_tagged_format(c, "recieving %d items from %d", bucket_counts[i], i);
    // increment the counters for the next round
    scount += my_bucket_counts[i];

    // print_tagged_format(c, "recieving totally %d items (at %d)", rcount, i);
    rcount += bucket_counts[i];
    // print_tagged_format(c, "recieving totally %d items (at %d)", rcount, i);
  }

  // print_tagged_format(c, "sending %d items", scount);
  // print_tagged_format(c, "recieving %d items", rcount);


  // and let's fill the buffers  with the right data
  MPI_Alltoallv(
      sends, my_bucket_counts, sdisplacements, MPI_INT,
      recieve, bucket_counts, rdisplacements, MPI_INT,
      MPI_COMM_WORLD);

  print_tagged(c, "gathered all buckets");

  // realloc all our old buckets
  // and copy the new info
  for (int i = 0; i < c->size; ++i) {
    if (i == c->rank)
      continue;

    c->buckets[i].data = realloc(c->buckets[i].data, bucket_sizes[i] * sizeof(int));
    memmove(c->buckets[i].data, recieve + rdisplacements[i], bucket_sizes[i] * sizeof(int));

    c->buckets[i].bucket_count = bucket_counts[i];
    c->buckets[i].bucket_size = bucket_sizes[i];
    c->buckets[i].max_bucket_size = my_bucket->max_bucket_size;
  }

  // free old resources
  free(my_bucket_sizes);
  free(my_bucket_counts);
  free(bucket_sizes);
  free(bucket_counts);
  free(sdisplacements);
  free(rdisplacements);
  free(send_counts);
}

// represents sorted data, 
// sorted is an array of 2N/P elems
// sorted_count is the index of the last empty spot in sorted
typedef struct {
  int sorted_count;
  int* sorted;
} sorted_t;

// Sort
sorted_t* sort_step(context_t* c) {
  stage(c, "sort step");

  const long unsigned int max_size = 2 * c->chunk_size;
  long unsigned int calculated_size = 0;

  for (int i = 0; i < c->size; ++i) {
    calculated_size += c->buckets[i].bucket_count;
  }

  if (calculated_size > max_size) {
    print_tagged_format(c, "sorted array w/ size greater than 2N/P, refusing to allocate: %lu over %lu ***FATAL***",
  calculated_size,
  max_size);
    exit(1);
  }

  int* sorted = calloc(calculated_size, sizeof(int));
  MPI_Offset sorted_index = 0;

  for (int i = 0; i < c->size; ++i) {
    if (c->buckets[i].bucket_count == 0)
      continue;

    if (sorted_index == max_size) {
      print_tagged(c, "sorted array overflow ***FATAL***");
      exit(1);
    }

    bucket_t* b = &c->buckets[i];

    for (int j = 0; j < b->bucket_count; ++j) {
      sorted[sorted_index++] = b->data[j];
    }
  }

  print_tagged(c, "created single sort array");

  bubblesort(sorted, 0, sorted_index - 1);

  sorted_t* s = calloc(1, sizeof(sorted_t));

  s->sorted_count = sorted_index;
  s->sorted = sorted;

  return s;
}

// Output (using concurrent writes)
void output_step(context_t* c, sorted_t* s) {
  stage(c, "output");

  // we need everyone w/ rank lower than us to compute the rank
  int* counts = calloc(c->size, sizeof(int));

  MPI_Allgather(&s->sorted_count, 1, MPI_INT, 
      counts, 1, MPI_INT,
      MPI_COMM_WORLD);

  MPI_Offset offset = 0;

  for (int i = 0; i < c->rank; ++i) {
    offset += counts[i];
  }

  // calculate byte offset
  offset *= sizeof(int);

  free(counts);

  MPI_File outfile;

  print_tagged_format(c, "opening %s and writing %d ints (%ld-%d)", 
      c->outfile, 
      s->sorted_count,
      offset,
      offset + (sizeof(int) * s->sorted_count));
  // print_array(s->sorted, s->sorted_count);

  // open the file for reading and writing
  MPI_File_open(MPI_COMM_WORLD, c->outfile,
      MPI_MODE_CREATE | MPI_MODE_WRONLY, 
      MPI_INFO_NULL, &outfile);

  // set the file view
  MPI_File_set_view(outfile, 
      offset, MPI_INT, MPI_INT,
      "native", MPI_INFO_NULL);

  // write to our view
  MPI_File_write(outfile, s->sorted, s->sorted_count, MPI_INT, MPI_STATUS_IGNORE);

  MPI_File_close(&outfile);
}

// do all the actual per-rank work
void work(context_t* c) {
  stage(c, "work");

  input_step(c);

  data_step(c);

  shuffle_step(c);

  sorted_t* s = sort_step(c);

  output_step(c, s);

  free(s->sorted);
  free(s);
}

// validate the context, ensure that
// N is divisible by P
void validate_context(context_t* c) {
  stage(c, "validating context...");

  int chunk_size = c->N / c->size;
  if ((chunk_size * c->size) != c->N) {
    printf("invalid file input size:\n");
    printf("P was %d, N was %lld\n", c->size, c->N);
    cleanup(c);
    exit(1);
  }
}

// MPI_Init
int main(int argc, char** argv) {
  MPI_Init(&argc, &argv);  

  if (argc != 3) {
    usage(1);
  }

  context_t* c = init_context(argv);

  validate_context(c);

  print_tagged(c, "process begin");

  work(c);

  print_tagged(c, "process complete");

  cleanup(c);
  MPI_Finalize();
}
