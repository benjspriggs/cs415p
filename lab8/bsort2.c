//------------------------------------------------------------------------- 
// This is supporting software for CS415P/515 Parallel Programming.
// Copyright (c) Portland State University.
//------------------------------------------------------------------------- 

// Bucket Sort (Sequential version 1)
//
// Usage: bsort2 B <infile> <outfile>   -- use B buckets to sort numbers from infile to outfile
//
// Assumption: B is a power of 2 (so that bucket distribution can be based 
//             on leading bits)
//
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#define MAX(X,Y) (((X) > (Y)) ? (X) : (Y))

// Specify data value range by bit-width 
//
#define DATABITS 13     // assume data are 13-bit integers: [0,8191] 

// Swap two array elements 
//
void swap(int *array, int i, int j) {
  if (i == j) return;
  int tmp = array[i];
  array[i] = array[j];
  array[j] = tmp;
}

// Bubble sort for the base cases
//
void bubblesort(int *array, int low, int high) {
  if (low >= high) return;
  for (int i = low; i <= high; i++)
    for (int j = i+1; j <= high; j++) 
      if (array[i] > array[j])
  swap(array, i, j);
}

// Initialize array
//
void init_array(int *a, int n) {
  srand(time(NULL));
  for (int i = 0; i < n; i++)
    a[i] = rand() % 8192;
}

// Verify that array elements are sorted
//
void verify_array(int *array, int N) {
  for (int i = 0; i < N-1; i++) {
    if (array[i]>array[i+1]) {
      printf("FAILED: array[%d]=%d, array[%d]=%d\n", 
       i, array[i], i+1, array[i+1]);
      return;
    }
  }
  printf("Result verified!\n");
}

// Print array
//
void print_array(int *array, int N) {
  for (int i = 0; i < N; i++) {
    printf("%4d ", array[i]);
    if (i % 12 == 11)
      printf("\n");
  }
  printf("\n");
}

// Return 1 if x is a power of 2
//
int isPower2(int x) {
  return !(x & (x - 1));
}

// Find bucket idx for an int value x of b bits
//
int bktidx(int b, int x, int B) {
  return x >> (b - (int)log2(B));
}

typedef struct {
  int N;
  int B;
  int bsize;
  int** bucket;
  int* bucket_count;
} bucket_magic_t;

bucket_magic_t* setup(FILE* fin, int B) {
  bucket_magic_t* r = malloc(sizeof(bucket_magic_t));

  fseek(fin, 0L, SEEK_END);
  r->B = B;
  r->N = ftell(fin);
  rewind(fin);

  // calculate the number of buckets and setup the
  // buckets and count
  r->bsize = MAX(64, 2 * (r->N / r->B));

  r->bucket = malloc(B * sizeof(int*));
  r->bucket_count = malloc(B * sizeof(int));

  for (int i = 0; i < B; ++i) {
    r->bucket[i] = malloc(r->bsize * sizeof(int));
    r->bucket_count[i] = 0;
  }

  return r;
}

// read ints from opened infile
// and store into s
void read_and_distribute(FILE* infile, bucket_magic_t* s) {
  int * array = malloc(s->N*sizeof(int));

  size_t actual_n = fread(array, 1, s->N, infile);

  if (actual_n != s->N) {
    printf("read didn't match the expected size, actual_n = %ld, s->N = %d\n", actual_n, s->N);
    exit(1);
  }

  // distribute the ints
  for (int i = 0; i < actual_n; ++i) {
    int k = bktidx(DATABITS, array[i], s->B);
    int bucket_index = s->bucket_count[k];
    s->bucket_count[k] += 1;
    s->bucket[k][bucket_index] = array[i];
  }
}

// Bucket sort -- main routine
//
int main(int argc, char **argv) {
  if (argc != 4) {
    printf("Usage: %s B <infile> <outfile>\n", argv[0]);
    exit(1);
  }

  // Get param B (numBkts) from command-line, verify it's a power of 2
  int B = atoi(argv[1]);
  if (!isPower2(B)) {
    printf("B (%d) must be a power of 2.\n", B);
    exit(1);
  }

  // read N from file
  FILE *infile = fopen(argv[2], "r");


  bucket_magic_t* buckets = setup(infile, B);

  read_and_distribute(infile, buckets);

  for (int k = 0; k < B; k++) {
    printf("Bucket%d[%d] ", k, buckets->bucket_count[k]);
    print_array(buckets->bucket[k], buckets->bucket_count[k]);
  }

  // Sort each bucket; copy result to result array
  int result[buckets->N];
  int rcnt = 0;
  for (int k = 0; k < B; k++) {
    int *data = buckets->bucket[k]; 
    int cnt = buckets->bucket_count[k];
    bubblesort(data, 0, cnt-1);
    for (int i = 0; i < cnt; i++)
      result[rcnt++] = data[i];
  }
  printf("Result[%d] ", rcnt);
  print_array(result, rcnt);

  // Verify the result array
  if (rcnt != buckets->N) {
    printf("Result size (%d) differs from input size (%d)\n", rcnt, buckets->N);
    exit(1);
  }
  verify_array(result, rcnt);

  // write the result to file
  FILE *outfile = fopen(argv[3], "w");

  int actual = fwrite(result, 1, buckets->N, outfile);

  if (actual != buckets->N) {
    printf("there was an error writing to file\n");
    exit(1);
  }

  printf("completed successfully\n");
}
